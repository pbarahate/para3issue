package reuseable;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.maven.shared.utils.StringUtils;

public class PropertiesOperation {

	static Properties prop = new Properties();
	public static String getPropertiesValues(String key) throws Exception {
		
		String proFilePath = System.getProperty("user.dir")+"/application.properties";
		FileInputStream fis = new FileInputStream(proFilePath);
		prop.load(fis);
		String value = prop.get(key).toString();
		
		if(StringUtils.isEmpty(value)) {
			throw new Exception("The Value is not specified for key:"+key+"in the properties files");
		}
		return value;
	}
	
	
}
